#pragma once

namespace Seti {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using System::SerializableAttribute;
	/// <summary>
	/// ������ ��� ReadLectures
	/// </summary>
	public ref class ReadLectures : public System::Windows::Forms::Form
	{
	public:
		ReadLectures(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~ReadLectures()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::MenuStrip^ menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^ �����ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ ����������������ToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
	private: AxAcroPDFLib::AxAcroPDF^ axAcroPDF1;



	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(ReadLectures::typeid));
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->�����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->axAcroPDF1 = (gcnew AxAcroPDFLib::AxAcroPDF());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->axAcroPDF1))->BeginInit();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(429, 555);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(94, 31);
			this->button1->TabIndex = 1;
			this->button1->Text = L"������� ����";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &ReadLectures::button1_Click);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->�����ToolStripMenuItem,
					this->����������������ToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(939, 24);
			this->menuStrip1->TabIndex = 2;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// �����ToolStripMenuItem
			// 
			this->�����ToolStripMenuItem->Name = L"�����ToolStripMenuItem";
			this->�����ToolStripMenuItem->Size = System::Drawing::Size(51, 20);
			this->�����ToolStripMenuItem->Text = L"�����";
			this->�����ToolStripMenuItem->Click += gcnew System::EventHandler(this, &ReadLectures::�����ToolStripMenuItem_Click);
			// 
			// ����������������ToolStripMenuItem
			// 
			this->����������������ToolStripMenuItem->Name = L"����������������ToolStripMenuItem";
			this->����������������ToolStripMenuItem->Size = System::Drawing::Size(132, 20);
			this->����������������ToolStripMenuItem->Text = L"���������� ������";
			this->����������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &ReadLectures::����������������ToolStripMenuItem_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			this->openFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &ReadLectures::openFileDialog1_FileOk);
			// 
			// axAcroPDF1
			// 
			this->axAcroPDF1->Enabled = true;
			this->axAcroPDF1->Location = System::Drawing::Point(12, 27);
			this->axAcroPDF1->Name = L"axAcroPDF1";
			this->axAcroPDF1->OcxState = (cli::safe_cast<System::Windows::Forms::AxHost::State^>(resources->GetObject(L"axAcroPDF1.OcxState")));
			this->axAcroPDF1->Size = System::Drawing::Size(915, 511);
			this->axAcroPDF1->TabIndex = 3;
			// 
			// ReadLectures
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(939, 613);
			this->Controls->Add(this->axAcroPDF1);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ReadLectures";
			this->ShowIcon = false;
			this->Text = L"����� ������";
			this->Load += gcnew System::EventHandler(this, &ReadLectures::ReadLectures_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->axAcroPDF1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ReadLectures_Load(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void openFileDialog1_FileOk(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e) {
	}
private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) 
{
	try
	{
		OpenFileDialog^ openFileDialog1 = gcnew OpenFileDialog();
		openFileDialog1->ShowDialog();
		axAcroPDF1->src = openFileDialog1->FileName;
	}
	catch (ArgumentException^  ex)
	{
		MessageBox::Show(ex->Message->ToString(), "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
	}
	catch (Exception^ ex)
	{
		MessageBox::Show(ex->Message->ToString(), "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);

	}

	

	/*
	OpenFileDialog^ openFileDialog1 = gcnew OpenFileDialog();
	openFileDialog1->ShowDialog();
	AxAcroPDFLib::AxAcroPDF^ axAcroPDF1 = gcnew AxAcroPDFLib::AxAcroPDF();
	axAcroPDF1->CreateControl();
	axAcroPDF1->src = openFileDialog1->FileName;*/



	/*if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{

		
		String^ filename = openFileDialog1->FileName;
		String^ content;
		try
		{
			StreamReader^ din = File::OpenText(filename);
			String^ str;
			int count = 0;
			while ((str = din->ReadLine()) != nullptr) {
				count++;
				content += str + "\r\n";
			}
			richTextBox1->Text = content;
		}
		catch (Exception^ e)
		{
			
			if (dynamic_cast<FileNotFoundException^>(e)) {
				richTextBox1->Text = "���� �� ������";
			}
			else
			{
				richTextBox1->Text = "�������� � ������� �����";
			}
		}
		//richTextBox1->LoadFile(openFileDialog1->FileName, RichTextBoxStreamType::UnicodePlainText);
	}*/
	

}
private: System::Void �����ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
private: System::Void ����������������ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
private: System::Void printPreviewControl1_Click(System::Object^ sender, System::EventArgs^ e) {
	
}
};
}
