#pragma once

namespace Seti {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Data::OleDb;
	using namespace System::Collections::Generic;

	/// <summary>
	/// ������ ��� PrepodForm
	/// </summary>
	public ref class PrepodForm : public System::Windows::Forms::Form
	{
	public:
		PrepodForm(void)
		{
			InitializeComponent();
			LoadTests(); //�������� ������� �����
			LoadAnswer(); //�������� ������� ������
			LoadQuestion(); //�������� ������� �������
			LoadTestsToCombobox(); // �������� �������� � ���������
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~PrepodForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^ menuStrip1;
	protected:
	private: System::Windows::Forms::ToolStripMenuItem^ �����ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ ����������������ToolStripMenuItem;
	private: System::Windows::Forms::TabControl^ tabControl1;
	private: System::Windows::Forms::TabPage^ tabPage1;












	private: System::Windows::Forms::Label^ label1;

	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button2;





	private: System::Windows::Forms::DataGridView^ dataGridView1;


	private: System::Windows::Forms::TabPage^ tabPage2;






















	private: System::Windows::Forms::Button^ button13;
	private: System::Windows::Forms::Button^ button14;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::Label^ label10;
	private: System::Windows::Forms::Button^ button9;
	private: System::Windows::Forms::Button^ button7;
	private: System::Windows::Forms::Button^ button12;
	private: System::Windows::Forms::Button^ button11;
	private: System::Windows::Forms::Button^ button10;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::DataGridView^ dataGridView3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ���_������;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ������������;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Test_Id;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Button^ button6;
	private: System::Windows::Forms::DataGridView^ dataGridView2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ���_�������;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ��������_������;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ IsTrueOrFalse;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ Id_�������;
	private: System::Windows::Forms::ComboBox^ comboBox1;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Button^ button8;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::TextBox^ testQuestion;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::TextBox^ answer1Text;
	private: System::Windows::Forms::CheckBox^ answer4IsTrue;
	private: System::Windows::Forms::TextBox^ answer2Text;
	private: System::Windows::Forms::CheckBox^ answer3IsTrue;
	private: System::Windows::Forms::TextBox^ answer3Text;
	private: System::Windows::Forms::CheckBox^ answer2IsTrue;
	private: System::Windows::Forms::TextBox^ answer4Text;
	private: System::Windows::Forms::CheckBox^ answer1IsTrue;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::Label^ label7;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ ���_�����;
private: System::Windows::Forms::DataGridViewTextBoxColumn^ ��������_�����;




















	private: System::ComponentModel::IContainer^ components;


	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->�����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->button14 = (gcnew System::Windows::Forms::Button());
			this->button13 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->���_����� = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->��������_����� = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button12 = (gcnew System::Windows::Forms::Button());
			this->button11 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->dataGridView3 = (gcnew System::Windows::Forms::DataGridView());
			this->���_������ = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->������������ = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Test_Id = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->dataGridView2 = (gcnew System::Windows::Forms::DataGridView());
			this->���_������� = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->��������_������ = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->IsTrueOrFalse = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Id_������� = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->testQuestion = (gcnew System::Windows::Forms::TextBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->answer1Text = (gcnew System::Windows::Forms::TextBox());
			this->answer4IsTrue = (gcnew System::Windows::Forms::CheckBox());
			this->answer2Text = (gcnew System::Windows::Forms::TextBox());
			this->answer3IsTrue = (gcnew System::Windows::Forms::CheckBox());
			this->answer3Text = (gcnew System::Windows::Forms::TextBox());
			this->answer2IsTrue = (gcnew System::Windows::Forms::CheckBox());
			this->answer4Text = (gcnew System::Windows::Forms::TextBox());
			this->answer1IsTrue = (gcnew System::Windows::Forms::CheckBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1->SuspendLayout();
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->tabPage2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->�����ToolStripMenuItem,
					this->����������������ToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1241, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// �����ToolStripMenuItem
			// 
			this->�����ToolStripMenuItem->Name = L"�����ToolStripMenuItem";
			this->�����ToolStripMenuItem->Size = System::Drawing::Size(51, 20);
			this->�����ToolStripMenuItem->Text = L"�����";
			this->�����ToolStripMenuItem->Click += gcnew System::EventHandler(this, &PrepodForm::�����ToolStripMenuItem_Click);
			// 
			// ����������������ToolStripMenuItem
			// 
			this->����������������ToolStripMenuItem->Name = L"����������������ToolStripMenuItem";
			this->����������������ToolStripMenuItem->Size = System::Drawing::Size(132, 20);
			this->����������������ToolStripMenuItem->Text = L"���������� ������";
			this->����������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &PrepodForm::����������������ToolStripMenuItem_Click);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Location = System::Drawing::Point(12, 27);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(1209, 523);
			this->tabControl1->TabIndex = 43;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->textBox1);
			this->tabPage1->Controls->Add(this->label10);
			this->tabPage1->Controls->Add(this->button14);
			this->tabPage1->Controls->Add(this->button13);
			this->tabPage1->Controls->Add(this->label1);
			this->tabPage1->Controls->Add(this->button4);
			this->tabPage1->Controls->Add(this->button2);
			this->tabPage1->Controls->Add(this->dataGridView1);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(1201, 497);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"���������� �����";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(150, 45);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(967, 20);
			this->textBox1->TabIndex = 44;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(30, 45);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(88, 13);
			this->label10->TabIndex = 43;
			this->label10->Text = L"�������� �����";
			// 
			// button14
			// 
			this->button14->Location = System::Drawing::Point(150, 103);
			this->button14->Name = L"button14";
			this->button14->Size = System::Drawing::Size(75, 23);
			this->button14->TabIndex = 42;
			this->button14->Text = L"��������";
			this->button14->UseVisualStyleBackColor = true;
			this->button14->Click += gcnew System::EventHandler(this, &PrepodForm::button14_Click);
			// 
			// button13
			// 
			this->button13->Location = System::Drawing::Point(561, 103);
			this->button13->Name = L"button13";
			this->button13->Size = System::Drawing::Size(75, 23);
			this->button13->TabIndex = 41;
			this->button13->Text = L"��������";
			this->button13->UseVisualStyleBackColor = true;
			this->button13->Click += gcnew System::EventHandler(this, &PrepodForm::button13_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(211, -38);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(81, 13);
			this->label1->TabIndex = 30;
			this->label1->Text = L"������ ������";
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(431, 103);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 28;
			this->button4->Text = L"�������";
			this->button4->UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(286, 103);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 27;
			this->button2->Text = L"��������";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &PrepodForm::button2_Click_1);
			// 
			// dataGridView1
			// 
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {
				this->���_�����,
					this->��������_�����
			});
			this->dataGridView1->Location = System::Drawing::Point(33, 253);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->Size = System::Drawing::Size(1084, 168);
			this->dataGridView1->TabIndex = 24;
			this->dataGridView1->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &PrepodForm::dataGridView1_CellClick);
			// 
			// ���_�����
			// 
			this->���_�����->HeaderText = L"���_�����";
			this->���_�����->Name = L"���_�����";
			this->���_�����->Width = 70;
			// 
			// ��������_�����
			// 
			this->��������_�����->HeaderText = L"��������_�����";
			this->��������_�����->Name = L"��������_�����";
			this->��������_�����->Width = 970;
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->button9);
			this->tabPage2->Controls->Add(this->button7);
			this->tabPage2->Controls->Add(this->button12);
			this->tabPage2->Controls->Add(this->button11);
			this->tabPage2->Controls->Add(this->button10);
			this->tabPage2->Controls->Add(this->label3);
			this->tabPage2->Controls->Add(this->dataGridView3);
			this->tabPage2->Controls->Add(this->label2);
			this->tabPage2->Controls->Add(this->button6);
			this->tabPage2->Controls->Add(this->dataGridView2);
			this->tabPage2->Controls->Add(this->comboBox1);
			this->tabPage2->Controls->Add(this->label4);
			this->tabPage2->Controls->Add(this->button8);
			this->tabPage2->Controls->Add(this->button5);
			this->tabPage2->Controls->Add(this->testQuestion);
			this->tabPage2->Controls->Add(this->button3);
			this->tabPage2->Controls->Add(this->answer1Text);
			this->tabPage2->Controls->Add(this->answer4IsTrue);
			this->tabPage2->Controls->Add(this->answer2Text);
			this->tabPage2->Controls->Add(this->answer3IsTrue);
			this->tabPage2->Controls->Add(this->answer3Text);
			this->tabPage2->Controls->Add(this->answer2IsTrue);
			this->tabPage2->Controls->Add(this->answer4Text);
			this->tabPage2->Controls->Add(this->answer1IsTrue);
			this->tabPage2->Controls->Add(this->label5);
			this->tabPage2->Controls->Add(this->label9);
			this->tabPage2->Controls->Add(this->label6);
			this->tabPage2->Controls->Add(this->label8);
			this->tabPage2->Controls->Add(this->label7);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(1201, 497);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"�������� ��������";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(482, 241);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(75, 23);
			this->button9->TabIndex = 110;
			this->button9->Text = L"��������";
			this->button9->UseVisualStyleBackColor = true;
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(482, 212);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(75, 23);
			this->button7->TabIndex = 109;
			this->button7->Text = L"��������";
			this->button7->UseVisualStyleBackColor = true;
			// 
			// button12
			// 
			this->button12->Location = System::Drawing::Point(482, 94);
			this->button12->Name = L"button12";
			this->button12->Size = System::Drawing::Size(75, 23);
			this->button12->TabIndex = 108;
			this->button12->Text = L"��������";
			this->button12->UseVisualStyleBackColor = true;
			// 
			// button11
			// 
			this->button11->Location = System::Drawing::Point(482, 123);
			this->button11->Name = L"button11";
			this->button11->Size = System::Drawing::Size(75, 23);
			this->button11->TabIndex = 107;
			this->button11->Text = L"�������";
			this->button11->UseVisualStyleBackColor = true;
			// 
			// button10
			// 
			this->button10->Location = System::Drawing::Point(482, 65);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(75, 23);
			this->button10->TabIndex = 106;
			this->button10->Text = L"��������";
			this->button10->UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(169, 18);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(126, 13);
			this->label3->TabIndex = 105;
			this->label3->Text = L"������ �������� �����";
			// 
			// dataGridView3
			// 
			this->dataGridView3->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView3->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
				this->���_������,
					this->������������, this->Test_Id
			});
			this->dataGridView3->Location = System::Drawing::Point(15, 34);
			this->dataGridView3->Name = L"dataGridView3";
			this->dataGridView3->Size = System::Drawing::Size(461, 138);
			this->dataGridView3->TabIndex = 104;
			// 
			// ���_������
			// 
			this->���_������->HeaderText = L"���_������";
			this->���_������->Name = L"���_������";
			this->���_������->Width = 70;
			// 
			// ������������
			// 
			this->������������->HeaderText = L"������������";
			this->������������->Name = L"������������";
			this->������������->Width = 180;
			// 
			// Test_Id
			// 
			this->Test_Id->HeaderText = L"Test_Id";
			this->Test_Id->Name = L"Test_Id";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(233, 175);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(118, 13);
			this->label2->TabIndex = 103;
			this->label2->Text = L"������ ������� �����";
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(482, 270);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(75, 23);
			this->button6->TabIndex = 102;
			this->button6->Text = L"�������";
			this->button6->UseVisualStyleBackColor = true;
			// 
			// dataGridView2
			// 
			this->dataGridView2->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView2->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
				this->���_�������,
					this->��������_������, this->IsTrueOrFalse, this->Id_�������
			});
			this->dataGridView2->Location = System::Drawing::Point(15, 191);
			this->dataGridView2->Name = L"dataGridView2";
			this->dataGridView2->Size = System::Drawing::Size(461, 144);
			this->dataGridView2->TabIndex = 101;
			// 
			// ���_�������
			// 
			this->���_�������->FillWeight = 50;
			this->���_�������->HeaderText = L"���_�������";
			this->���_�������->Name = L"���_�������";
			this->���_�������->Width = 70;
			// 
			// ��������_������
			// 
			this->��������_������->HeaderText = L"��������_������";
			this->��������_������->Name = L"��������_������";
			this->��������_������->Width = 200;
			// 
			// IsTrueOrFalse
			// 
			this->IsTrueOrFalse->HeaderText = L"IsTrueOrFalse";
			this->IsTrueOrFalse->Name = L"IsTrueOrFalse";
			this->IsTrueOrFalse->Width = 75;
			// 
			// Id_�������
			// 
			this->Id_�������->HeaderText = L"Id_�������";
			this->Id_�������->Name = L"Id_�������";
			this->Id_�������->Width = 70;
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(763, 34);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(271, 21);
			this->comboBox1->TabIndex = 100;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(633, 34);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(88, 13);
			this->label4->TabIndex = 87;
			this->label4->Text = L"�������� �����";
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(959, 380);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(75, 23);
			this->button8->TabIndex = 99;
			this->button8->Text = L"��������";
			this->button8->UseVisualStyleBackColor = true;
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(858, 380);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(75, 23);
			this->button5->TabIndex = 98;
			this->button5->Text = L"�������";
			this->button5->UseVisualStyleBackColor = true;
			// 
			// testQuestion
			// 
			this->testQuestion->Location = System::Drawing::Point(763, 82);
			this->testQuestion->Name = L"testQuestion";
			this->testQuestion->Size = System::Drawing::Size(271, 20);
			this->testQuestion->TabIndex = 82;
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(763, 380);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 97;
			this->button3->Text = L"���������";
			this->button3->UseVisualStyleBackColor = true;
			// 
			// answer1Text
			// 
			this->answer1Text->Location = System::Drawing::Point(763, 137);
			this->answer1Text->Name = L"answer1Text";
			this->answer1Text->Size = System::Drawing::Size(271, 20);
			this->answer1Text->TabIndex = 83;
			// 
			// answer4IsTrue
			// 
			this->answer4IsTrue->AutoSize = true;
			this->answer4IsTrue->Location = System::Drawing::Point(1067, 314);
			this->answer4IsTrue->Name = L"answer4IsTrue";
			this->answer4IsTrue->Size = System::Drawing::Size(90, 17);
			this->answer4IsTrue->TabIndex = 96;
			this->answer4IsTrue->Text = L"����������";
			this->answer4IsTrue->UseVisualStyleBackColor = true;
			// 
			// answer2Text
			// 
			this->answer2Text->Location = System::Drawing::Point(763, 194);
			this->answer2Text->Name = L"answer2Text";
			this->answer2Text->Size = System::Drawing::Size(271, 20);
			this->answer2Text->TabIndex = 84;
			// 
			// answer3IsTrue
			// 
			this->answer3IsTrue->AutoSize = true;
			this->answer3IsTrue->Location = System::Drawing::Point(1067, 254);
			this->answer3IsTrue->Name = L"answer3IsTrue";
			this->answer3IsTrue->Size = System::Drawing::Size(90, 17);
			this->answer3IsTrue->TabIndex = 95;
			this->answer3IsTrue->Text = L"����������";
			this->answer3IsTrue->UseVisualStyleBackColor = true;
			// 
			// answer3Text
			// 
			this->answer3Text->Location = System::Drawing::Point(763, 252);
			this->answer3Text->Name = L"answer3Text";
			this->answer3Text->Size = System::Drawing::Size(271, 20);
			this->answer3Text->TabIndex = 85;
			// 
			// answer2IsTrue
			// 
			this->answer2IsTrue->AutoSize = true;
			this->answer2IsTrue->Location = System::Drawing::Point(1067, 197);
			this->answer2IsTrue->Name = L"answer2IsTrue";
			this->answer2IsTrue->Size = System::Drawing::Size(90, 17);
			this->answer2IsTrue->TabIndex = 94;
			this->answer2IsTrue->Text = L"����������";
			this->answer2IsTrue->UseVisualStyleBackColor = true;
			// 
			// answer4Text
			// 
			this->answer4Text->Location = System::Drawing::Point(763, 311);
			this->answer4Text->Name = L"answer4Text";
			this->answer4Text->Size = System::Drawing::Size(271, 20);
			this->answer4Text->TabIndex = 86;
			// 
			// answer1IsTrue
			// 
			this->answer1IsTrue->AutoSize = true;
			this->answer1IsTrue->Location = System::Drawing::Point(1067, 139);
			this->answer1IsTrue->Name = L"answer1IsTrue";
			this->answer1IsTrue->Size = System::Drawing::Size(90, 17);
			this->answer1IsTrue->TabIndex = 93;
			this->answer1IsTrue->Text = L"����������";
			this->answer1IsTrue->UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(633, 82);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(82, 13);
			this->label5->TabIndex = 88;
			this->label5->Text = L"����� �������";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(633, 311);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(46, 13);
			this->label9->TabIndex = 92;
			this->label9->Text = L"����� 4";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(633, 131);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(46, 13);
			this->label6->TabIndex = 89;
			this->label6->Text = L"����� 1";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(633, 252);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(46, 13);
			this->label8->TabIndex = 91;
			this->label8->Text = L"����� 3";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(633, 194);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(46, 13);
			this->label7->TabIndex = 90;
			this->label7->Text = L"����� 2";
			// 
			// PrepodForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1241, 663);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"PrepodForm";
			this->ShowIcon = false;
			this->Text = L"����� �������������";
			this->Load += gcnew System::EventHandler(this, &PrepodForm::PrepodForm_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->tabPage1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}

#pragma endregion
	private: System::Void ����������������ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void �����ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void dataGridView1_CellContentClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^ e);

	private: System::Void PrepodForm_Load(System::Object^ sender, System::EventArgs^ e) {
	}

	private: System::Void button7_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e);

	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e);

	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void LoadTests();

	private: System::Void LoadAnswer();

	private: System::Void LoadQuestion();

	private: System::Void button8_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button7_Click_1(System::Object^ sender, System::EventArgs^ e);
	private: System::Void dataGridView2_CellContentClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^ e);


	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button3_Click_1(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button9_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button5_Click_1(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button6_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button10_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button12_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void button11_Click(System::Object^ sender, System::EventArgs^ e);
	private: System::Void bindingSource1_CurrentChanged(System::Object^ sender, System::EventArgs^ e) {
	}

	private: System::Void LoadTestsToCombobox();

	private: System::Void addTestQuestion_Click_1(System::Object^ sender, System::EventArgs^ e);


	private: System::Int32 AddTestAnswer(Int32 questionId, String^ answerText, bool isTrue);
	private:System::Int32 AddQuestion(String^ question, Int32 testId);
	private: System::Void button13_Click(System::Object^ sender, System::EventArgs^ e);
private: System::Void button14_Click(System::Object^ sender, System::EventArgs^ e);
private: System::Void button2_Click_1(System::Object^ sender, System::EventArgs^ e);
private: System::Void button3_Click_2(System::Object^ sender, System::EventArgs^ e) {
}

private: System::Void dataGridView1_CellClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^ e) {
	textBox1->Text = dataGridView1->CurrentCell->Value->ToString();
	
}
};
}
