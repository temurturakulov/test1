#pragma once
#include "TestForm.h"
#include "Answers.h"
#include <string>
#include <list>
#include <vector>
#include <memory>
#include <string>
#include <string>
#include <iostream>

using namespace std;

class TestQuestions
{

	std::string QuestionText;
	//System::Collections::Generic::List<int> snswers;
	//list<Answers> answers;
	string Text;
	bool IstrueOrFalse;

public:
	TestQuestions(string questionText ) {
		this->QuestionText = questionText;
	}

	string GetQuestionText();
	list<Answers> GetAnswers();

	void SetAnswers(list<Answers> answers);
	void SetQuestionText(string text);

	bool GetIsTrueOrfalse();
	string GetText();

};

